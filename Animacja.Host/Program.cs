﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Ninject;
using Ninject.Extensions.Wcf;
using Animacja.Implementation;
using System.ServiceModel.Web;
using System.ServiceModel;
using System.ServiceModel.Description;


namespace Animacja.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<IAnimuj>().ToSelf().InSingletonScope();
            kernel.Bind<IKrec>().To<IAnimuj>();
            kernel.Bind<IZnikaj>().To<IAnimuj>();

            using (var host = kernel.Get<NinjectServiceHost<IAnimuj>>())
            {
                host.Open();

                Console.WriteLine("serwer dostępny {0}", host.BaseAddresses[0]);
                Console.WriteLine("Wciśnij [Enter] by zatrzymać prace serwera.");
                Console.ReadLine();

                host.Close();
            }
        }
        //private static WebServiceHost CreateServiceHost()
        //{
        //    //Tworzenie hosta
        //    var serviceType = typeof(Animacja.Implementation.IAnimuj);
        //    var serviceUri = new Uri(@"http://localhost:9221");
        //    var host = new WebServiceHost(serviceType, serviceUri);

        //    var serviceEndPoint = host.AddServiceEndpoint(typeof(IKrec), new WebHttpBinding(), "");
        //    host.Description.Endpoints[0].Behaviors.Add(new WebHttpBehavior { HelpEnabled = true });
        //    return host;
        //}
    }
}
