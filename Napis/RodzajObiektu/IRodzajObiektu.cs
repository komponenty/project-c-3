﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obiekty.Implementation;

namespace RodzajObiektu
{
    public interface IRodzajObiektu
    {
        Obiekt obiekt
        {
            get;
            set;
        }
        int dlugosc
        {
            get;
            set;
        }
        int szerokosc
        {
            get;
            set;
        }

        Obiekt NowyObiekt(Obiekt obiekty);
        void Polozenie(int dlugosc, int szerokosc);
    }
}
