﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using Napis.Contaract;
using Animacja.Implementation;
using Animacja.Contract;

namespace Napis.Implementation
{
    public class CObiekt : INapis
    {
        protected object obiekt;
        protected IAnimacja animacja;

        public CObiekt()
        {
        }

        public CObiekt(string napis)
        {
            CNapis napisy = new CNapis(napis);
            obiekt = napisy;
        }

        public object Dodaj(object obiekt)
        {
            this.obiekt = obiekt;
            return obiekt;
        }

        public object Usun(object obiekt)
        {
            this.obiekt = obiekt;
            this.obiekt = null;
            return this.obiekt;
        }

        public Font RozmiarLiter(int rozmiar)
        {
            CNapis napis = new CNapis();
            return napis.RozmiarLiter(rozmiar);
        }
        public Color KolorLiter(Color kolor)
        {
            CNapis napis = new CNapis();
            return napis.KolorLiter(kolor);
        }
        public Font TypCzcionki(string czcionka)
        {
            CNapis napis = new CNapis();
            return napis.TypCzcionki(czcionka);
        }
        public void TworzAnimacje(Bitmap obraz, bool krec, bool znikaj)
        {
            IAnimuj animuj = new IAnimuj();
            if (krec)
            {
                animuj.AnimujKrec(obraz);
            }
            if (znikaj)
            {
                animuj.AnimujZnikaj(obraz);
            }
        }
        public IAnimacja TworzAnimacje()
        {  
            return animacja;
        }
       public void Otrorz()
        {
           Console.Write("OK");
        }
    }
}