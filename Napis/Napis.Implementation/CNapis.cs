﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Napis.Implementation
{
    public class CNapis
    {
        protected string napis;
        protected Color kolor = Color.Black;
        protected int rozmiar = 12;
        protected string czcionka = "Arial";
        protected Font styl = new Font("Arial", 12);
        //protected Label tekst;

        public CNapis(string napis)
        {
            this.napis = napis;
            //this.tekst.Text = this.napis;
            //this.tekst.ForeColor = this.kolor;
            //this.tekst.Font = this.styl;
            //this.tekst.Size = new Size(napis.Length, rozmiar + 2);
        }


        public CNapis() { }

        public Font RozmiarLiter(int rozmiar)
        {
            this.rozmiar = rozmiar;
            this.styl = new Font(czcionka, this.rozmiar);
            //this.tekst.Font = this.styl;
            //this.tekst.Size = new Size(napis.Length, this.rozmiar + 2);
            return this.styl;
        }

        public Color KolorLiter(Color kolor)
        {
            this.kolor = kolor;
            //this.tekst.ForeColor = this.kolor;
            return this.kolor;
        }

        public Font TypCzcionki(string czcionka)
        {
            this.czcionka = czcionka;
            this.styl = new Font(this.czcionka, rozmiar);
            //this.tekst.Font = this.styl;
            return this.styl;
        }
    }
}
