﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.ServiceModel;
using System.Drawing;

namespace Napis.Contaract
{
   [ServiceContract]
    public interface INapis
    {
       [OperationContract]
        object Dodaj(object obiekt);
       [OperationContract]
        object Usun(object obiekt);
       [OperationContract]
       void Otrorz();
       [OperationContract]
       Font RozmiarLiter(int rozmiar);
       [OperationContract]
       Color KolorLiter(Color kolor);
       [OperationContract]
       Font TypCzcionki(string czcionka);
    }
}
