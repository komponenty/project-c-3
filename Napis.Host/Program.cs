﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Ninject;
using Ninject.Extensions.Wcf;
using Napis.Implementation;
using Napis.Contaract;
using Animacja.Contract;
using System.ServiceModel.Web;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Napis.Host
{
  public class Program
    {
        //static void Main(string[] args)
        //{
         
        //    IKernel kernel = new StandardKernel();
        //    kernel.Bind<CNapis>().ToSelf().InSingletonScope();
        //    kernel.Bind<CObiekt>().ToSelf().InSingletonScope();
        //    kernel.Bind<INapis>().To<CObiekt>();          
        //    kernel.Bind<IAnimacja>().ToMethod(ctx =>
        //    {
        //        var factory = new ChannelFactory<IAnimacja>("animacja");
        //        return factory.CreateChannel();
        //    });
        //    using (var host2 = kernel.Get<NinjectServiceHost<CObiekt>>())
        //    {

        //        host2.Open();

        //        Console.WriteLine("The service is ready at {0}", host2.BaseAddresses[0]);
        //        Console.WriteLine("Press <Enter> to stop the service.");
        //        Console.ReadLine();

        //        host2.Close();
        //    }
        //}
        static void Main(string[] args)
        {
            Console.WriteLine("Usługa DBManager - trwa włączanie");
            var host = CreateServiceHost();
            host.Open();
            Console.Clear();
            Console.WriteLine("Usługa DBManager została włączona.");
            Console.WriteLine("Aby zakończyć, naciśnij ENTER");
            Console.ReadLine();
            host.Close();
        }

        private static WebServiceHost CreateServiceHost()
        {
            //Tworzenie hosta
            var serviceType = typeof(CObiekt);
            var serviceUri = new Uri(@"http://localhost:9001");
            var host = new WebServiceHost(serviceType, serviceUri);

            var serviceEndPoint = host.AddServiceEndpoint(typeof(INapis), new WebHttpBinding(), "");
            host.Description.Endpoints[0].Behaviors.Add(new WebHttpBehavior { HelpEnabled = true });
            return host;
        }

    }
}
