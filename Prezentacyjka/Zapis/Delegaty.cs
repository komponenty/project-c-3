﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prezentacyjka
{

    public class Delegaty
    {
        public delegate Boolean MojDelegat(String log, IList<System.Windows.Forms.Panel> plansza);

        ISerializacja ser;
        DoSerializacji g = new DoSerializacji();
        public Boolean Zapisz(String log, IList<System.Windows.Forms.Panel> plansza)
        {
            ser = new ISerializacja(log,plansza);
            SaveFileDialog okienko = new SaveFileDialog();
            okienko.Filter = "Pliki tekstowe (*.txt)|*.txt"; 
            if (okienko.ShowDialog() == DialogResult.OK)
            {
              Settings.Save(g.Przyg(), okienko.FileName);
              return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean Otworz(String log, IList<System.Windows.Forms.Panel> plansza)
        {
            ser = new ISerializacja(log, plansza);
            OpenFileDialog okienko = new OpenFileDialog();
            okienko.Filter = "Pliki tekstowe (*.txt)|*.txt";
            if (okienko.ShowDialog() == DialogResult.OK)
            {
               ser =  Settings.Load(okienko.FileName);
               return true;
            }
            else
            {
                return false;
            }
        }
        public String Zmiana()
        {
            try
            {
                return ser.login;
            }
            catch (Exception c) { return "Błąd"; }
        }
        public IList<System.Windows.Forms.Panel> Zmiana2()
        {
             try
            {
              
            return ser.zbior;
            }
             catch (Exception c) 
             { 
                 IList<System.Windows.Forms.Panel> pusta = new List<System.Windows.Forms.Panel>() { }; 
                 return pusta; 
             }
        }


    }
}
