﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Prezentacyjka
{
    class DoSerializacji : MainWindow
    {
        public String login;
        public IList<System.Windows.Forms.Panel> zbior = new List<System.Windows.Forms.Panel> { };
        
        public ISerializacja Przyg() 
        {
            this.login = get_login();
            this.zbior = get_plansza();
            ISerializacja n = new ISerializacja(login,zbior);
            return n;
        }
    }

}
