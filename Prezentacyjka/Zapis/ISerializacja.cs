﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Diagnostics;


namespace Prezentacyjka
{
[Serializable]
   public class ISerializacja
    {
    public String login;
    public IList<System.Windows.Forms.Panel> zbior = new List<System.Windows.Forms.Panel> { };

    public ISerializacja(String login, IList<System.Windows.Forms.Panel> zbior)
    {
        this.login = login;
        this.zbior = zbior;
    }
       
    }
public class Settings
{
    const int VERSION = 1;
   public  static void Save(ISerializacja settings, string fileName)
    {
        Stream stream = null;
        try
        {
            IFormatter formatter = new BinaryFormatter();
            stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, VERSION);
            formatter.Serialize(stream, settings);
        }
        catch
        {
            // do nothing, just ignore any possible errors
        }
        finally
        {
            if (null != stream)
                stream.Close();
        }

    }

   public  static ISerializacja Load(string fileName)
    {
        Stream stream = null;
        ISerializacja settings = null;
        try
        {
            IFormatter formatter = new BinaryFormatter();
            stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
            int version = (int)formatter.Deserialize(stream);
            Debug.Assert(version == VERSION);
            settings = (ISerializacja)formatter.Deserialize(stream);
      
        }
        catch
        {
            // do nothing, just ignore any possible errors
        }
        finally
        {
            if (null != stream)
                stream.Close();
        }
      
        return settings;
    }
}

}
