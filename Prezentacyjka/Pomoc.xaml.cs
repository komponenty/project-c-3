﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prezentacyjka
{
    /// <summary>
    /// Interaction logic for Pomoc.xaml
    /// </summary>
    public partial class Pomoc : Window
    {
        public Pomoc()
        {
            InitializeComponent();
        }

        private void text_dodaj(object sender, RoutedEventArgs e)
        {
            MenuItem n = (MenuItem)sender;
            switch (n.Name)
            {
                case "sl":
                    {
                        text.Text = "Aby dodać nowy slajd kliknij przycisk \"Nowy slajd\". Slajdy pojawają się w określonej kolejności, która jest niezmienna. \n Aby usunać slajd kliknij prawym przyciskiem myszy na przycisku \"Nowy slajd\" i wybierz opcję \'Usuń slajd\" slajd zostanie usuniety i wszystko co na nim było również. Mozna przywrócić sam pusty slajd wybierająć opcję \"Przywróć slajd\".";
                        break;
                    }
                case "ob":
                    {
                        text.Text = "Nowe obrazy dodajemy wybierając odpowiednią ścierzke  dostępu do obrazu. Należy pamiętać o podaniu odpowiedniego połozenia.";
                        break;
                    }
                case "zk":
                    {
                        text.Text = "Aby zmienić kolor okna należ wybrać w menu opcję Widok -> Kolorystyka.";
                        break;
                    }
                case "log":
                    {
                        text.Text = "Możasz zalogować się poprzez bazę danych, albo pracować offline. Przy starcie programu należy wybrać odpowiednią opcję.";
                        break;
                    }
                case "dod":
                    {
                        text.Text = "Uwaga przy dodawaniu nowych obiektów na slajdy należy się upewnić, czy wybraliśmy odpowiedni slajd z listy!";
                        break;
                    }
                default:
                    break;
            }
        }
    }
}
