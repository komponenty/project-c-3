﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.ServiceModel.Web;
using System.ServiceModel;
using System.ServiceModel.Description;

using MenadzerLogowania;
using QuizMaker.DBManager.Contract;
namespace Prezentacyjka
{
    /// <summary>
    /// Interaction logic for Start.xaml
    /// </summary>
    public partial class Start : Window
    {
        private IDBManager nn;
        private ILoguj n;
        public Start()
        {  
            InitializeComponent();
        }
        public Start(IDBManager nn, ILoguj n)
        {
            this.nn = nn;
            this.n = n;
            //var serviceUri = @"http://localhost:9202";
            //var serviceChannelFactory = new ChannelFactory<IDBManager>(new WebHttpBinding(), serviceUri);
            //serviceChannelFactory.Endpoint.Behaviors.Add(new WebHttpBehavior());
            //nn = serviceChannelFactory.CreateChannel();
            //serviceUri = @"http://localhost:8733/SystemZarzadzania/MenadzerLogowania/";
            //var serviceChannelFactory2 = new ChannelFactory<ILoguj>(new WebHttpBinding(), serviceUri);
            //serviceChannelFactory.Endpoint.Behaviors.Add(new WebHttpBehavior());
            //n = serviceChannelFactory2.CreateChannel();

            InitializeComponent();
        }
  
        private void offline_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow("offline");
            mainWindow.Show();
            Close();
        }

        private void Zaloguj_Click(object sender, RoutedEventArgs e)
        {
            nn = new QuizMaker.DBManager.Impl.DBManager();
            n = new MenadzerLogowania.MenadzerLogowania();

            if (nn.VerifyUser(loginB.Text, passwordB.Text))
            {
                if (n.ZalogujCE(loginB.Text, passwordB.Text))
                {
                    var mainWindow = new MainWindow(loginB.Text);
                    mainWindow.Show();
                    Close();
                }
            }
            else
            {
                MessageBox.Show("Niepoprawny login lub hasło");
            }
        }
        private void nowe_konto(object sender, RoutedEventArgs e)
        {

            nn = new QuizMaker.DBManager.Impl.DBManager();
            if (nn.AddUser(loginB.Text, passwordB.Text))
            {
                nn.InsertData(new List<string> { "Login", "Password" }, new List<string> { loginB.Text, passwordB.Text }, "Users");

                IList<string> col = new List<string>() { "ID", "Login", "Password" };
                IList<string> war = new List<string>() { };

                war.Add(loginB.Text);
                war.Add(passwordB.Text);
                if (nn.InsertData(col, war, "Users"))
                    MessageBox.Show("Zostałeś dodany do listy użytkowników");
            }
            else
            {
                MessageBox.Show("Błąd! Spróbuj ponownie.");
            }



        }
    }
}
