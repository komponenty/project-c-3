﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Ninject;
using Obraz.Contaract;
using Napis.Contaract;
using Animacja.Contract;
using MenadzerLogowania;
using QuizMaker.DBManager.Contract;
using Kontenerek;


namespace Prezentacyjka
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Open(object sender, StartupEventArgs e)
        { 
            var container = Kontener.Kontenery;
            var obiekt = container.Get<IObraz>();
            var napis = container.Get<INapis>();
            var log = container.Get<ILoguj>();
            var men = container.Get<IDBManager>();
            var start = new Start(men,log);


            Obraz.Host.Program n = new Obraz.Host.Program();
            Napis.Host.Program nn = new Napis.Host.Program();
           

            start.Show();
        }
    }
}
