﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel.Web;
using System.ServiceModel;
using System.ServiceModel.Description;
using Ninject;

using QuizMaker.DBManager.Contract;

namespace Prezentacyjka
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window 
    {

        String log;
        string tekst = "Błąd";
        int i = 1;
        double hight;
        double width;
        bool krec = false;
        bool znikaj = false;
        IList<System.Drawing.Bitmap> nazwa = new List<System.Drawing.Bitmap>();
        IList<System.Windows.Forms.Panel> listaplansza = new List<System.Windows.Forms.Panel> { }; 
        IList<System.Windows.Forms.Integration.WindowsFormsHost> listawindow = new List<System.Windows.Forms.Integration.WindowsFormsHost> { };
        IList<Button> listabutton = new List<Button> { }; 
        IList<System.Drawing.Color> col;

        #region Main
        public MainWindow()
        {
            InitializeComponent();
        }
        public MainWindow(string log)
        {
            InitializeComponent();
           
            this.log = log;
            logowanie.Text = logowanie.Text + log;
            listabutton = new List<Button> { nowyslajd1, nowyslajd2, nowyslajd3, nowyslajd4, nowyslajd5, nowyslajd6, nowyslajd7, nowyslajd8, nowyslajd9, nowyslajd10 };
            listaplansza = new List<System.Windows.Forms.Panel> { plansza1, plansza2, plansza3, plansza4, plansza5, plansza6, plansza7, plansza8, plansza9, plansza10 };       
            listawindow = new List<System.Windows.Forms.Integration.WindowsFormsHost> { windowsform, windowsform2, windowsform3, windowsform4, windowsform5, windowsform6, windowsform7, windowsform8, windowsform9, windowsform10 };
            RozmiaryIkonek();
            comboBox1.Items.Add("Slajd " + 1);
            comboBox1.SelectedIndex = comboBox1.Items.Count - 1;

            col = new List<System.Drawing.Color>() { System.Drawing.Color.Black, System.Drawing.Color.Blue, System.Drawing.Color.Crimson, System.Drawing.Color.DeepPink, System.Drawing.Color.Gold, System.Drawing.Color.Green, System.Drawing.Color.LightSkyBlue, System.Drawing.Color.LightYellow, System.Drawing.Color.Magenta, System.Drawing.Color.Orange, System.Drawing.Color.Purple, System.Drawing.Color.Red, System.Drawing.Color.Teal, System.Drawing.Color.Yellow };
            foreach (var i in col)
            {
                kolorczcionki.Items.Add(i);
            }
            
            System.Drawing.FontFamily [] fonts;
            fonts = System.Drawing.FontFamily.Families;
            foreach (System.Drawing.FontFamily font in fonts)
                rodzajczcionki.Items.Add(font.Name);
            for (int i = 1; i < 50; i++)
            {
                wielkoscczcionki.Items.Add(i);
            }

       
        }
        #endregion

        public Obraz.Contaract.IObraz Obie() 
        {
            var serviceUri = @"http://localhost:9000";
            var serviceChannelFactory = new ChannelFactory<Obraz.Contaract.IObraz>(new WebHttpBinding(), serviceUri);
            serviceChannelFactory.Endpoint.Behaviors.Add(new WebHttpBehavior());
            Obraz.Contaract.IObraz dbm = serviceChannelFactory.CreateChannel();


            //var container = Kontenerek.Kontener.Kontenery;
            //var obiekty = container.Get<Obraz.Contaract.IObraz>();
            //return obiekty;
            return dbm;
        }
        public Napis.Contaract.INapis Obie2()
        {
            var serviceUri = @"http://localhost:9001";
            var serviceChannelFactory = new ChannelFactory<Napis.Contaract.INapis>(new WebHttpBinding(), serviceUri);
            serviceChannelFactory.Endpoint.Behaviors.Add(new WebHttpBehavior());
            Napis.Contaract.INapis dbm = serviceChannelFactory.CreateChannel();


           // var container = Kontenerek.Kontener.Kontenery;
           // var obiekty = container.Get<Napis.Contaract.INapis>();
           //return obiekty;
            return dbm;
        }


      

        public string get_login()
        {
            return log;
        }  
        public IList<System.Windows.Forms.Panel> get_plansza()
        {
            return listaplansza;
        } 
        private void Zamknij_Click(object sender, RoutedEventArgs e)
        {
            Close();
            Application.Current.Shutdown();
        }
 
        #region Widocznosc
        public void widocznosc_Unchecked(object sender, RoutedEventArgs e)
        {
            dodawanie.Visibility = System.Windows.Visibility.Collapsed;
        }
        public void widocznosc_Checked(object sender, RoutedEventArgs e)
        {
            dodawanie.Visibility = System.Windows.Visibility.Visible;
        }
        private void Zielony_Click(object sender, RoutedEventArgs e)
        {
            RadialGradientBrush myBrush = new RadialGradientBrush();

            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255,113,120,33), 0.974));
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255,224,245,16), 0.228));
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255, 179,196,12), 0.474));
            myBrush.GradientStops.Add(new GradientStop(Colors.White, 0.026));
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255,166,182,14), 0.725));
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255, 145,156,35), 0.947));
            zielony.Background = myBrush;

            Style myStyl = new Style();
            System.Windows.Thickness d = new Thickness(2, 2, 2, 2);
            SolidColorBrush back = new SolidColorBrush(Color.FromArgb(255, 179,196,12));
            SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(255, 194,213,11));
            myStyl.Setters.Add(new Setter(Button.BackgroundProperty, back));
            myStyl.Setters.Add(new Setter(Button.BorderBrushProperty, brush));
            myStyl.Setters.Add(new Setter(Button.MarginProperty, d)); 
            comboBox1.Style = myStyl;
            napis.Style = myStyl;
            obraz.Style = myStyl;
            menu1.Style = myStyl;
            rodzajczcionki.Style = myStyl;
            kolorczcionki.Style = myStyl;
            wielkoscczcionki.Style = myStyl;
            dod1.Style = myStyl;
            dod2.Style = myStyl;
            anu1.Style = myStyl;
            anu2.Style = myStyl;
            
        }
        private void Niebieski_Click(object sender, RoutedEventArgs e)
        {
            RadialGradientBrush myBrush = new RadialGradientBrush();
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255, 25,100,156), 0.974));
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255, 16,175,245), 0.228));
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255,12,182,196), 0.474));
            myBrush.GradientStops.Add(new GradientStop(Colors.White, 0.026));
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(94,14,217,220), 0.725));
            myBrush.GradientStops.Add(new GradientStop(Color.FromArgb(255, 35,82,156), 0.947));
            zielony.Background = myBrush;

            Style myStyl = new Style();
            Button v = new Button();
            System.Windows.Thickness d = new Thickness(2,2,2,2);
            SolidColorBrush back = new SolidColorBrush(Color.FromArgb(94, 14, 217, 220));
            SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(255, 35, 82, 156));
            myStyl.Setters.Add(new Setter(Button.BackgroundProperty, back)); 
            myStyl.Setters.Add(new Setter(Button.BorderBrushProperty, brush));
            myStyl.Setters.Add(new Setter(Button.MarginProperty, d)); 
            comboBox1.Style = myStyl;
            napis.Style = myStyl;
            obraz.Style = myStyl;
            menu1.Style = myStyl;
            rodzajczcionki.Style = myStyl;
            kolorczcionki.Style = myStyl;
            wielkoscczcionki.Style = myStyl;
            dod1.Style = myStyl;
            dod2.Style = myStyl;
            anu1.Style = myStyl;
            anu2.Style = myStyl;

        } 
        private void zielony_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            RozmiaryIkonek();
        } 
        public void RozmiaryIkonek()
        {
            
            double Window_withe = zielony.Width;
            double Window_higth = zielony.Height;

            h.Maximum = Window_higth * 0.81;
            hn.Maximum = Window_higth * 0.81;
            w.Maximum = Window_withe - 200;
            wn.Maximum = Window_withe - 200;

            dockPanel5.Width = Window_withe*0.97;
            dodawanie.Width = Window_withe*0.15;
            logowanie.Width = Window_withe * 0.97;
            dockmenu.Width = Window_withe * 0.97;
            slajdy.Width = Window_withe*0.600;
            wrapPanel1.Width = Window_withe*0.13;
            labelslajd.Width = Window_withe * 0.13;
            labelobiekt.Width = Window_withe * 0.13;

            foreach (var j in listawindow)
            {
                j.Width = Window_withe-200 ;
                j.Height = Window_higth*0.81;
            }

            dockmenu.Height = Window_higth * 0.1;
            dockPanel5.Height =Window_higth*0.815;
        }
       #endregion
        #region Slajd
        private void nowyslajd_Click(object sender, RoutedEventArgs e)
        {
           if (i<10)
           {
                listabutton[i].Visibility = System.Windows.Visibility.Visible;
                i++;
                comboBox1.Items.Add("Slajd " +i.ToString());
                comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
               
           }          
        }
        private void usunslajd_Click(object sender, RoutedEventArgs e)
        {
               listabutton[(int)jed.Value].Visibility = System.Windows.Visibility.Collapsed;            
               listaplansza[(int)jed.Value].Dispose();
               int p = (int)jed.Value + 1;
               comboBox1.Items.Remove("Slajd " +p);
               comboBox1.SelectedIndex = comboBox1.Items.Count + 1;
        } 
        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider n = (Slider)sender;
            switch (n.Name)
            {
                case "jed":
                    {
                       ktory.Content = n.Value +1;
                       break;
                    }
                case "dwa":
                    {
                       ktory2.Content =n.Value + 1;
                       break;
                    }
                case "h":
                    {
                        hight =  n.Value;
                        lh.Content = n.Value;
                        break;
                    }
                case "w":
                    {
                        width = n.Value;
                        lw.Content = n.Value;
                        break;
                    }
                case "hn":
                    {
                        hight = n.Value;
                        lhn.Content = n.Value;
                        break;
                    }
                case "wn":
                    {
                        width = n.Value;
                        lwn.Content = n.Value;
                        break;
                    }
                default:
                    break;
            }
            
           
        }     
        private void przyw_Click(object sender, RoutedEventArgs e)
        {
            listabutton[(int)dwa.Value].Visibility = System.Windows.Visibility.Visible;
            int p = (int)dwa.Value + 1;
            comboBox1.Items.Add("Slajd " + p);
            comboBox1.SelectedIndex = comboBox1.Items.Count - 1;  
        }
        private void pokaz_slajd(object sender, RoutedEventArgs e)
        {
            Button n = (Button)sender;
            System.Windows.Forms.Button p = new System.Windows.Forms.Button();
            plansza1.Controls.Add(p);
            foreach (var i in listawindow)
            {
                i.Visibility = System.Windows.Visibility.Collapsed;
            }
            int ile = int.Parse(n.Content.ToString());
            listawindow[ile-1].Visibility = System.Windows.Visibility.Visible;
           
        }
        private void wybor_slajdu_combo(object sender, SelectionChangedEventArgs e)
        {
            ComboBox n = (ComboBox) sender;        
            foreach (var i in listawindow)
            {
                i.Visibility = System.Windows.Visibility.Collapsed;
            }
              try
                {
            listawindow[n.SelectedIndex].Visibility = System.Windows.Visibility.Visible;
                }
              catch (ArgumentOutOfRangeException z)
              {
                 
              }
        } 
         #endregion
        #region Dodawanie
        private void Napis_Click(object sender, RoutedEventArgs e)
        {
            dodawanienapis.Visibility = System.Windows.Visibility.Visible;
            obraz.IsEnabled = false;
        }    
        private void Dodaj_Napis(object sender, RoutedEventArgs e)
        {
            dodawanienapis.Visibility = System.Windows.Visibility.Collapsed;
            napis.IsEnabled = true;
            obraz.IsEnabled = true;
        
             System.Windows.Forms.Label m = new System.Windows.Forms.Label();
             m.Text = textbox1.Text;

              Napis.Contaract.INapis obiekt2 = Obie2();

      //        try
      //        {
           //       obiekt2.Otrorz();
     //         }
     //         catch (Exception c)
     //         {
    //            obiekt2 = new Napis.Implementation.CObiekt();
    //          }
              m.Location = new System.Drawing.Point((int)width, (int)hight);
             
            try
            {
                System.Drawing.Font styl = obiekt2.TypCzcionki(rodzajczcionki.SelectedValue.ToString());
                styl = obiekt2.RozmiarLiter((int)wielkoscczcionki.SelectedIndex + 1);
        
                System.Drawing.Color kol = obiekt2.KolorLiter( col[kolorczcionki.SelectedIndex]);
                m.Font = styl;
                m.ForeColor = kol;
                m.BackColor = System.Drawing.Color.Transparent;
                m.Size = new System.Drawing.Size(tekst.Length *(int)wielkoscczcionki.SelectedIndex, (int)wielkoscczcionki.SelectedIndex * 2);
        
            }
            catch (NullReferenceException z)
            {
      
            }
            try
            {
                listaplansza[comboBox1.SelectedIndex].Controls.Add(m);
                m.BringToFront();
            }
            catch (ArgumentOutOfRangeException z)
            {
                MessageBox.Show("Wybierz slajd!");
            }
        }
        private void Obraz_Click(object sender, RoutedEventArgs e)
        {
            dodawanieobraz.Visibility = System.Windows.Visibility.Visible;
            napis.IsEnabled = false;
        }
        private void Dodaj_Obraz(object sender, RoutedEventArgs e)
        {
           
            dodawanieobraz.Visibility = System.Windows.Visibility.Collapsed;
            napis.IsEnabled = true;
            obraz.IsEnabled = true;
            var obiekty = Obie();   
     //       try
     //       {
             //   obiekty.Otrorz();
     //       }
     //       catch (Exception c)
     //       {
                
    //            obiekty = new Obraz.Implementation.CObiekt();
    //        }
            try
            {
                System.Drawing.Bitmap obrazy = obiekty.DodajZdjecie();
                int wwight = obrazy.Width;
                int hhidht = obrazy.Height;
                System.Windows.Forms.PictureBox n = new System.Windows.Forms.PictureBox();
                n.BackgroundImage = obrazy;
                n.Size = new System.Drawing.Size(wwight, hhidht);
                n.Location = new System.Drawing.Point((int)width, (int)hight);
                try
                {
                    listaplansza[comboBox1.SelectedIndex].Controls.Add(n);
                    n.BringToFront();
                }
                catch (ArgumentOutOfRangeException z)
                {
                    MessageBox.Show("Utwórz nowy slajd!");
                }

            }
            catch (Exception c)
            {
 
            }
        }
        private void Anuluj(object sender, RoutedEventArgs e)
        {
            dodawanieobraz.Visibility = System.Windows.Visibility.Hidden;
            dodawanienapis.Visibility = System.Windows.Visibility.Hidden;
            napis.IsEnabled = true;
            obraz.IsEnabled = true;
        }
        private void pomoc_Click(object sender, RoutedEventArgs e)
        {
            var pom = new Prezentacyjka.Pomoc();
            pom.Show();
        }
        #endregion


        private void Zapisz(object sender, RoutedEventArgs e) 
        {
            //IDBManager n = new QuizMaker.DBManager.Impl.DBManager();
            //IList<string> columny = new List<string>() { "login", "plansze" };
            //IList<string> wartości = new List<string>() { };
            //wartości.Add(log);
            //wartości.Add(listaplansza.ToString());
            //String tabela = "Plansze";


            //if (n.InsertData(columny, wartości, tabela))
            //{
            //    MessageBox.Show("Zapisano");
            //}
            //else
            //{
            //    MessageBox.Show("Błąd zapisu");
            //}
            Delegaty d = new Delegaty();
            Delegaty.MojDelegat zapisz= new Delegaty.MojDelegat(d.Zapisz);
            Boolean a = zapisz(log, listaplansza);
            if (!a)
            {
                MessageBox.Show("Błąd zapisu");
            }
        }
        
        private void Otworz(object sender, RoutedEventArgs e) 
        {
            //IDBManager n = new QuizMaker.DBManager.Impl.DBManager();
            //IList<string> columny = new List<string>() { "plansza", "login" };
            //String tabela = "Plansze";
            //try
            //{
            //    IList<IDictionary<string, string>> odt = n.GetData(columny, tabela);

            //    foreach (var o in odt)
            //    {
            //        foreach (var j in o)
            //        {
            //            if (j.Value == log)
            //            {
            //                listaplansza = (List<System.Windows.Forms.Panel>)(object)j.Key;
            //            }
            //        }

            //    }
            //}
            //catch { MessageBox.Show("Błąd odczytu"); }
           Delegaty d = new Delegaty();
           Delegaty.MojDelegat otworz = new Delegaty.MojDelegat(d.Otworz);
           Boolean a = otworz(log, listaplansza);
           if (a)
           {
             this.log = d.Zmiana();
             logowanie.Text = "Zalogowany jako "+ log;
       //      this.listaplansza = d.Zmiana2();
           }
           else
           {
               MessageBox.Show("Błąd odczytu");
           }
        }

      

        private void krecenie_Checked(object sender, RoutedEventArgs e)
        {
            var obiekt = Obie();
            krec = true;
           
         //   obiekt.TworzAnimacje(nazwa[i], krec, znikaj);

        }

        private void znikanie_Checked(object sender, RoutedEventArgs e)
        {
            var obiekt = Obie();

            znikaj = true;
          //  obiekt.TworzAnimacje(nazwa[i], krec, znikaj);
        }
    }
}