﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Kontenerek.Moduly;

namespace Kontenerek
{
    public class Kontener
    {
        private static IKernel kontener;

        public static IKernel Kontenery
        {
            get
            {
                if (kontener == null)
                    kontener = ConfigureApp();
                return kontener;
            }
        }

        private static IKernel ConfigureApp()
        {
            var kernel = new StandardKernel(
                new ModulAnimacji(),
                new ModulNapisu(),
                new ModulObrazu(),
                new ModulBaza(),
                new ModulLog()
                );
            return kernel;
        }
    }
}
