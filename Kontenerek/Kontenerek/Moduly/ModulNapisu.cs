﻿using Ninject;
using Ninject.Modules;
using System.ServiceModel;

using Napis.Implementation;

namespace Kontenerek.Moduly
{
    class ModulNapisu : NinjectModule
    {
        public override void Load()
        {
            Bind<Napis.Implementation.CObiekt>().ToSelf().InSingletonScope();
            Bind<Napis.Contaract.INapis>().To<Napis.Implementation.CObiekt>().InSingletonScope();
            //Bind<Napis.Contaract.INapis>().ToMethod(ctx =>
            //{
            //    var factory = new ChannelFactory<Napis.Contaract.INapis>("nap");
            //    return factory.CreateChannel();
            //}).InSingletonScope();
        }
    }
}
