﻿using Ninject;
using Ninject.Modules;

using QuizMaker.DBManager.Contract;
using QuizMaker.DBManager.Impl;


namespace Kontenerek.Moduly
{
    class ModulBaza : NinjectModule
    {
        public override void Load()
        {
            Bind<DBManager>().ToSelf().InSingletonScope();       
            Bind<IDBManager>().To<DBManager>().InSingletonScope();
        }
    }
}
