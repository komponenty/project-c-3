﻿using Ninject;
using Ninject.Modules;
using System.ServiceModel;

using Obraz.Implementation;

namespace Kontenerek.Moduly
{
    class ModulObrazu: NinjectModule
    {
        public override void Load()
        {
            Bind<Obraz.Implementation.CObiekt>().ToSelf().InSingletonScope();
           Bind<Obraz.Contaract.IObraz>().To<Obraz.Implementation.CObiekt>().InSingletonScope();
           //Bind<Obraz.Contaract.IObraz>().ToMethod(ctx =>
           //{
           //    var factory = new ChannelFactory<Obraz.Contaract.IObraz>("obz");
           //    return factory.CreateChannel();
           //}).InSingletonScope();
        }
    }
}
