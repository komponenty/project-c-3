﻿using Ninject;
using Ninject.Modules;

using Animacja.Implementation;

namespace Kontenerek.Moduly
{
    class ModulAnimacji : NinjectModule
    {
        public override void Load()
        {
            Bind<IKrec>().To<IAnimuj>().InSingletonScope();
            Bind<IZnikaj>().To<IAnimuj>().InSingletonScope();

        }
    }
}
