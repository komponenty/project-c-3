﻿using Ninject;
using Ninject.Modules;

using MenadzerLogowania;

namespace Kontenerek.Moduly
{
    class ModulLog : NinjectModule
    {
        public override void Load()
        {
            Bind<MenadzerLogowania.MenadzerLogowania>().ToSelf().InSingletonScope();
            Bind<MenadzerLogowania.ILoguj>().To<MenadzerLogowania.MenadzerLogowania>().InSingletonScope();
        }
    }
}
