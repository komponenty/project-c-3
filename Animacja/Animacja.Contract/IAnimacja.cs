﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Animacja.Implementation;
using System.ServiceModel.Web;
using System.ServiceModel;

namespace Animacja.Contract
{
    [ServiceContract]
    public interface IAnimacja
    {
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        IAnimuj animuj();
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [OperationContract]
        void TworzAnimacje(System.Drawing.Bitmap obraz, bool krec, bool znikaj);
    }
}
