﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Animacja.Implementation
{
    public interface IZnikaj
    {
        string animacja
        {
            get;
            set;
        }
        void AnimujZnikaj(Bitmap obrazek);
    }
}
