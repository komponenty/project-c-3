﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Animacja.Implementation
{
    public interface IKrec
    {
        string animacja
        {
            get;
            set;
        }
        Bitmap AnimujKrec(Bitmap obrazek);
    }
}
