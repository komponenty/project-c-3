﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Animacja.Implementation
{
    public class IAnimuj : IKrec, IZnikaj
    {
        private List<string> animacja;
        private string animuj;
        private IKrec krec;
        private IZnikaj znikaj;
        void Animuj(string animacja)
        {
            animuj = animacja;
            this.animacja.Add(animuj);
        }

        string IKrec.animacja
        {
            get
            {
                return animuj;
            }
            set
            {
                animuj = krec.animacja;
            }
        }

        public Bitmap AnimujKrec(Bitmap obrazek)
        {
            Bitmap obraz=obrazek;
            Graphics graf = Graphics.FromImage(obraz);
            graf.TranslateTransform((float)obraz.Height, (float)obraz.Width);
            graf.RotateTransform(90);
            graf.TranslateTransform(-(float)obraz.Width / 2, -(float)obraz.Height / 2);
            graf.DrawImage(obraz, new Point(0, 0));
            //this.animacja.Add(obraz.ToString());
            return obraz;
        }

        string IZnikaj.animacja
        {
            get
            {
                return animuj; ;
            }
            set
            {
                animuj = znikaj.animacja; ;
            }
        }

        public void AnimujZnikaj(Bitmap obrazek)
        {
            Bitmap obraz=obrazek;
            PictureBox box = new PictureBox();
            box.Image = obraz;
            box.Hide();
            //this.animacja.Add(obraz.ToString());
        }
    }
}
