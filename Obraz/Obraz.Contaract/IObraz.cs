﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.ServiceModel;
using System.Drawing;

namespace Obraz.Contaract
{
   [ServiceContract]
    public interface IObraz
    {

       [OperationContract]
        object Dodaj(object obiekt);
       [OperationContract]
        object Usun(object obiekt);
       [OperationContract]
       void Otrorz();
       [OperationContract]
       Bitmap DodajZdjecie();
    }
}
