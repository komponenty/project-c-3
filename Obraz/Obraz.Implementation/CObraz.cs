﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Obraz.Implementation
{
    public class CObraz
    {
        protected Bitmap zdjecie;

        public Bitmap DodajZdjecie()
        {
            OpenFileDialog okienko = new OpenFileDialog();
            okienko.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.JPEG;*.PNG)|*.BMP;*.JPG;*.GIF;*.JPEG;*.PNG";
            if (okienko.ShowDialog() == DialogResult.OK)
            {
                this.zdjecie = new Bitmap(okienko.FileName);
            }
            else
            {
                this.zdjecie = null;
            }
            return this.zdjecie;
        }

        public void UsunZdjecie(Bitmap zdjecie)
        {
            this.zdjecie = zdjecie;
            this.zdjecie = null;
        }
    }
}
