﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using Obraz.Contaract;
using Animacja.Implementation;
using Animacja.Contract;

namespace Obraz.Implementation
{
    public class CObiekt : IObraz
    {
        protected object obiekt;
        protected IAnimacja animacja;

        public CObiekt()
        {
        }

        public CObiekt(CObraz obraz)
        {
            obiekt = obraz;
        }
        public object Dodaj(object obiekt)
        {
            this.obiekt = obiekt;
            return obiekt;
        }

        public object Usun(object obiekt)
        {
            this.obiekt = obiekt;
            this.obiekt = null;
            return this.obiekt;
        }

        public Bitmap DodajZdjecie()
        {
            CObraz obraz = new CObraz();
            Bitmap zdjecie=obraz.DodajZdjecie();
            return zdjecie;
        }
        public void UsunZdjecie(Bitmap zdjecie)
        {
            CObraz obraz = new CObraz();
            obraz.UsunZdjecie(zdjecie);
        }
        public void TworzAnimacje(Bitmap obraz, bool krec, bool znikaj)
        {
            IAnimuj animuj = new IAnimuj();
            if (krec)
            {
                animuj.AnimujKrec(obraz);
            }
            if (znikaj)
            {
                animuj.AnimujZnikaj(obraz);
            }
        }
        public IAnimacja TworzAnimacje()
        {  
            return animacja;
        }
        public void Otrorz()
        {
            Console.Write("OK");
        }
    }
}